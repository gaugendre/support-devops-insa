let assert = require('chai').assert;
let routes = require('../lib/routes');

/*
* Test la fonction computeTotal dans tout les sens
*/
let total = routes.computeTotal([1], [1]);
assert.equal(total, 1, "Total is alright");

total = routes.computeTotal([0], [1]);
assert.equal(total, 0, "Total is alright");

total = routes.computeTotal([1], [0]);
assert.equal(total, 0, "Total is alright");

total = routes.computeTotal([1000], [1]);
assert.equal(total, 1000, "Total is alright");

total = routes.computeTotal([1,1,1,1,1], [1,0,0,0,0]);
assert.equal(total, 1, "Total is alright");

total = routes.computeTotal([1,0,0,0,0], [1,1,1,1,1]);
assert.equal(total, 1, "Total is alright");

total = routes.computeTotal([1.2], [1]);
assert.equal(total, 1.2, "Total is alright");


/*
* Test la fonction applyTax dans tout les sens
*/

total = routes.applyTax(1, 1.2);
assert.equal(total, 1.2, "Total is alright");

total = routes.applyTax(1.2, 1.2);
assert.equal(total, 1.44, "Total is alright");

total = routes.applyTax(1.2, 1);
assert.equal(total, 1.2, "Total is alright");


/*
* Test la fonction applyReduction dans tout les sens
*/

total = routes.applyReduction(50000, 'STANDARD');
assert.equal(total, 42500, "Total is alright");

total = routes.applyReduction(10000, 'STANDARD');
assert.equal(total, 9000, "Total is alright");

total = routes.applyReduction(7000, 'STANDARD');
assert.equal(total, 6510, "Total is alright");

total = routes.applyReduction(5000, 'STANDARD');
assert.equal(total, 4750, "Total is alright");

total = routes.applyReduction(1000, 'STANDARD');
assert.equal(total, 970, "Total is alright");


total = routes.applyReduction(1000, 'PAY THE PRICE');
assert.equal(total, 1000, "Total is alright");


total = routes.applyReduction(1000, 'HALF PRICE');
assert.equal(total, 500, "Total is alright");
