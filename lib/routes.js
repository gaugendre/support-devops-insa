function handle400(message, res) {
  console.warn(message);
  return res.status(400).json({
    error: message,
  });
}

function computeTotal(prices, quantities) {
  let total = 0;
  for (let i = 0; i < prices.length; i++) {
    total += prices[i] * quantities[i];
  }
  return total;
}
exports.computeTotal = computeTotal;

function applyTax(total, tax) {
  return total * tax;
}
exports.applyTax = applyTax;

function applyReduction(total, reduction) {
  if (reduction === 'STANDARD') {
    if (total >= 50000) {
      total *= 0.85;
    }
    else if (total >= 10000) {
      total *= 0.90
    }
    else if (total >= 7000) {
      total *= 0.93
    }
    else if (total >= 5000) {
      total *= 0.95
    }
    else if (total >= 1000) {
      total *= 0.97
    }
  }
  else if (reduction === 'HALF PRICE') {
    total *= 0.5
  }
  return total;
}
exports.applyReduction = applyReduction;


exports.order = function order(req, res, next) {
  console.log(req.body);
  let taxes = {
    DE: 1.2,
    UK: 1.21,
    FR: 1.2,
    IT: 1.25,
    ES: 1.19,
    PL: 1.21,
    RO: 1.2,
    NL: 1.2,
    BE: 1.24,
    EL: 1.2,
    CZ: 1.19,
    PT: 1.23,
    HU: 1.27,
    SE: 1.23,
    AT: 1.22,
    BG: 1.21,
    DK: 1.21,
    FI: 1.17,
    SK: 1.18,
    IE: 1.21,
    HR: 1.23,
    LT: 1.23,
    SI: 1.24,
    LV: 1.2,
    EE: 1.22,
    CY: 1.21,
    LU: 1.25,
    MT: 1.2,
  };
  let allowedReductions = [
    'STANDARD',
    'HALF PRICE',
    'PAY THE PRICE'
  ];

  if (!Array.isArray(req.body.quantities)) {
    let message = "quantities is not an array";
    return handle400(message, res);
  }
  if (!Array.isArray(req.body.prices)) {
    let message = "prices is not an array";
    return handle400(message, res);
  }
  if (req.body.prices.length !== req.body.quantities.length) {
    let message = "quantities and prices not the same length";
    return handle400(message, res);
  }
  if (!req.body.hasOwnProperty('country')) {
    let message = "no country";
    return handle400(message, res);
  }
  if (Object.keys(taxes).indexOf(req.body.country) === -1) {
    let message = "country doesn't exist";
    return handle400(message, res);
  }
  if (!req.body.hasOwnProperty('reduction')) {
    let message = "no reduction";
    return handle400(message, res);
  }
  if (allowedReductions.indexOf(req.body.reduction) === -1) {
    let message = "reduction does not exist";
    return handle400(message, res);
  }

  let total = computeTotal(req.body.prices, req.body.quantities);
  total = applyTax(total, taxes[req.body.country]);
  total = applyReduction(total, req.body.reduction);

  let response = {
    total: total
  };
  console.log("Returning", response);
  res.json(response);
};

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};
