# Tuto
## Comment mettre en place son environnement de dev ?
1. S'inscrire sur gitlab.com
2. Forker le projet https://gitlab.com/eauc/support-devops-insa
3. Cloner le projet `git clone <url>`
4. Sur sa machine, se placer dans le dossier du projet (cloné)
4. Installer Docker https://docs.docker.com/install/
5. Lancer les commandes suivantes
```
docker build -t monprojet .
docker run -p 3000:3000 -v $PWD:/app -v /app/node_modules monprojet npm run nodemon
```

(remplacer `monprojet` par le nom qu'on veut donner à l'image)

## Comment lancer les tests ?
1. Vérifier que l'image est à jour : `docker build -t monprojet .`
2. Lancer les tests :

```
docker run -v $PWD:/app -v /app/node_modules --rm monprojet npm run test
```

(remplacer `monprojet` par le nom qu'on a donné à l'image)


## Comment fonctionne la CI ?
Lors d'un push sur la branche `master` dans Gitlab, une pipeline est déclenchée.
Cette pipeline fait tourner trois tâches :
- build : construit l'image Docker et la push sur le registry de Gitlab
- test : lance les tests contenus dans l'image pour s'assurer qu'on ne met pas en ligne des régressions
- deploy : déploie l'image en production sur Heroku

On ne passe de l'étape `N` à l'étape `N+1` que si l'étape `N` s'est déroulée correctement (code de sortie 0)
