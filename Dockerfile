FROM node:8

WORKDIR /app/
COPY package.json package-lock.json ./
RUN npm install
COPY test ./test/
COPY server.js ./
COPY lib ./lib/

EXPOSE 3000

CMD ["npm", "start"]
